﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace yt_music_downloader
{
    public partial class Form1 : Form
    {
        Config cfg;
        public Form1()
        {
            InitializeComponent();
            cfg = new Config();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> temp = new List<string>();
            for (int i = 0; i < getSongs.Split('\n').Length; i++)
                temp.Add(getSongs.Split('\n')[i]);

            DownloadSong s = new DownloadSong(temp, cfg);
            s.Run();
        }

        private string getSongs
        {
            get
            {
                string tmp = null;
                if (this.songs.InvokeRequired)
                    this.songs.Invoke(new Action(() => tmp = songs.Text));
                else
                    tmp = this.songs.Text;
                return tmp;
            }
        }
    }
}
