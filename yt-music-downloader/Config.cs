﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace yt_music_downloader
{
    class Config
    {

        private JToken j = null;

        public struct cfg_str
        {
            public string save_path;
        }

        public Config()
        {
            Init();
        }

        private void Init()
        {
            if (!File.Exists("config.json"))
                if (!CreateFile())
                    return; //TODO: Error handling

            try
            {
                j = JObject.Parse(File.ReadAllText("config.json"));
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                //TODO: Error handling
            }
        }

        private bool CreateFile()
        {
            try
            {
                cfg_str str = new cfg_str
                {
                    save_path = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic) + @"\"
                };
                using (StreamWriter file = new StreamWriter("config.json"))
                {
                    string temp = JsonConvert.SerializeObject(str);
                    file.Write(temp);
                };
                return true;
            } catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                return false;
                //TODO: Exception handling
            }
        }

        public object read(string obj)
        {
            if (j == null)
                return null; //TODO: Error handling
            try
            {
                return j[obj].ToObject<object>();
            } catch(Exception e)
            {
                //TODO: Error handling
                return null;
            }
        }
    }
}
