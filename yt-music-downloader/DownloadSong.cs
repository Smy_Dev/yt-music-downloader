﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VideoLibrary;
using System.Text.RegularExpressions;

namespace yt_music_downloader
{
    class DownloadSong
    {
        private Config cfg;

        private bool canRun = false;
        private List<string> list = null;

        public DownloadSong(List<string> list, Config cfg)
        {
            this.list = list;
            this.cfg = cfg;
            canRun = true;
        }

        public void Run()
        {
            if (!canRun && cfg != null && list != null)
                return; //TODO: Error handling

            for(int i = 0; i < list.Count; i++)
            {
                var yt = YouTube.Default;
                var v = yt.GetVideo(list[i]);
                string sng_nm = normal_name(v.FullName);
                File.WriteAllBytes(cfg.read("save_path").ToString() + sng_nm, v.GetBytes());
            }
        }

        private string normal_name(string name)
        {
            try
            {
                return name.Replace(" - YouTube", "");
            } catch(Exception e)
            {
                return name;
            }
        }

        private static string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }
    }
}
